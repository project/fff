--------------------------------------------------------------------------------
  Forbidden File Format module Readme
  https://www.drupal.org/project/fff
--------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION
3. REQUIREMENTS
4. CREDITS

1. ABOUT
========

Provides setting for the file upload fields, which allows to upload all types of files except of specified.
For example you can deny to upload files with types js, exe, bat, com but all other types of files will be allowed for upload.
It can be useful if you want to give some freedom for users who want to upload something, but protect users who want to download something from dangerous files.

2. INSTALLATION
===============

Install as usual, see https://www.drupal.org/node/1897420 for further information.
Contact me with any questions.

3. REQUIREMENTS
===============

This module requires:
Module file (from Drupal core)

4. CREDITS
==========

Project page: http://drupal.org/project/fff

Maintainers:
Eugene Ilyin - https://www.drupal.org/user/1767626
